package com.north47.dependencyreader;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DependencyReaderApplication {

    public static void main(String[] args) {
        SpringApplication.run(DependencyReaderApplication.class, args);
    }

}
